<?php

Class Route {

	var $routes = array();

	public function add_route($signature, $route) {
		//check if the signature or route is null
		if (!is_null($signature) && !is_null($route)) {
			//if not null then store

			$this -> routes[$signature] = $route;
		}
	}

	public function execute_route($route) {

		//check what methods are allowed and locate a match
		$request_method = strtolower($_SERVER['REQUEST_METHOD']);
		$execute_allowed = false;
		$route_name = null;

		if (array_key_exists("GET:" . $route, $this -> routes) && $request_method == "GET") {
			$execute_allowed = true;
			$route_name = "GET:" . $route;

		} else if (array_key_exists("POST:" . $route, $this -> routes) && $request_method == "POST") {
			$execute_allowed = true;
			$route_name = "POST:" . $route;

		} else if (array_key_exists("ALL:" . $route, $this -> routes) && $request_method == "ALL") {
			$execute_allowed = true;
			$route_name = "ALL:" . $route;
		}
		//if there is nothing else, then just do this.

		else if (array_key_exists($route, $this -> routes)) {
			$execute_allowed = true;
			$route_name = $route;
		}

		//check if the route exists
		if ($execute_allowed && !is_null($route_name)) {
			//execute the route

			$route_signature = $route;

			$route_function = $this -> routes[$route_name];

			if (call_user_func($route_function)) {

				return true;
			}

		} else {
			//throw an error.

			if (method_exists($this, error)) {

			}

		}

	}

	public function error() {

		print("An Error Occured");

	}

}
?>