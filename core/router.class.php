<?php

Class Router {

	var $routes = array();

	public function add_route($route_object) {
		if (!is_null($route_object)) {
			//add it to the list of known routes.
			$this -> routes[] = $route_object;
		}

	}

	public function navigate() {

		$requestVals = array();
		$request_method = strtolower($_SERVER['REQUEST_METHOD']);
		$path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $path_info;
		$path_info = isset($_SERVER['ORIG_PATH_INFO']) ? $_SERVER['ORIG_PATH_INFO'] : $path_info;

		$route_params = explode('/', $path_info);

		foreach ($this->routes as $route_object) {

			if ($route_object -> execute_route($route_params[1])) {

				return true;
			}

		}
		return false;
	}

}
?>