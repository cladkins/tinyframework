<?php

define("INAPP",true);

//setup the app
require dirname(__FILE__)."/core/input.class.php";
require dirname(__FILE__)."/core/route.class.php";
require dirname(__FILE__)."/core/router.class.php";
require dirname(__FILE__)."/app/comp/display/Twig/Autoloader.php"; 
require dirname(__FILE__)."/app/comp/display/display_loader.class.php";


$router=new Router();

//pages
require dirname(__FILE__)."/app/system/theme/default/controller.php";

$route=new Controller();




$router->add_route($route);


$router->navigate();



?>