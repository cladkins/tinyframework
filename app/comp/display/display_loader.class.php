<?php
if (!defined('INAPP'))
	exit('No direct script access allowed');

Class DisplayLoader {

	static $twigTemplates = null;
	//Twig Templates (Array or String) of locations.
	static $exposedFunctions = array();
	//Array of function names, abbreviated and the full name available in the Display.
	/**
	 * Render Display
	 * @param files - The location of the view, or views, <Br>if an array is specified then the view matching the selected_file will be loaded and displayed.
	 * @param selected_file - The selected view file.
	 * @param display_data - Data passed to the template as an associative array.
	 * @param cache - The location of the cache, or null for no cache. If the application is dynamic cache must be set to null.
	 * @param dont_display - If true then dont display the parsed template, just return it as a string value.
	 * @param rules- a list of replace rules to run on the raw html template usefull for updating raw html.<font color="red"> Do not call this every time, use once to make html and then disable the flag!</font>
	 */
	public static function renderDisplay($files, $selected_file, $display_data, $cache = null, $dont_display = false, $rules = NULL) {
		
		if($display_data==null){
			$display_data=array();
		}
		
		
		//if the rules are in array format then build the template using the rules.

		if (is_array($rules)) {

			self::updateTemplateURLS($files . "/" . $selected_file, $rules);
		}

		Twig_Autoloader::register();

		$loader = new Twig_Loader_Filesystem($files);
		$twig = new Twig_Environment($loader, array('cache' => $cache));

		//Iterate through available functions
		foreach (DisplayLoader::$exposedFunctions as $controller_name => $function_obj) {

			//var_dump($function_obj);
			//echo "short_name:".$function_obj->getShort_function_name()."<br>";
			//echo "long_name:".$function_obj->getTrue_function_name()."<br>";
			//$twig->addFilter($function_obj->getShort_function_name(), new Twig_Filter_Function($function_obj->getTrue_function_name()));

			$twig -> addFunction($function_obj -> getShort_function_name(), new Twig_Function_Function($function_obj -> getTrue_function_name()));
		}

		$template = $twig -> loadTemplate($selected_file);

		if ($dont_display) {
			return $template -> render($display_data);
		} else {
			print($template -> render($display_data));
		}

	}

	/**
	 * Expose A PHP function so it will be available for use in display templates.
	 * @param function_short_name - Name used to access PHP function within the template.
	 * @param function_name - True name of the PHP function you want to use in the template.
	 */
	public static function exposeFunction($function_short_name, $function_name, $controller_name) {

		//echo "Function Short Name:".$function_short_name."<br>";
		//echo "Function Name:".$function_name."<br>";
		//echo "Controller Name:".$controller_name."<br>";
		//var_dump(func_get_args());
		DisplayLoader::$exposedFunctions[$controller_name] = new ExposedFunction($controller_name, $function_short_name, $function_name);
	}

	public static function updateTemplateURLS($template_name, $rules = NULL) {

		//first check to see if the file needs updating.

		//split the file into a array using newline vals.
		$file_data = file_get_contents($template_name);

		$first_line = explode("\n",$file_data);
		$first_line=$first_line[0];
		print("efwe:".$first_line);

		if ($first_line == "<!-- TWIG NO_REBUILD -->") {

			foreach ($rules as $key => $value) {
				$key = base64_decode($key);
				
				
				$arr=unserialize($key);
				if(is_array($arr)){
					
					print($arr[0]);
					
					
					
				}
				
				
				//$file_data = str_replace($key, $key . $value, $file_data);
			}

			$fh = fopen($template_name . ".fixed", 'w') or die("can't save generated template file!");
			$result = fwrite($fh, "<!-- TWIG NO_REBUILD -->\n".$file_data);
			fclose($fh);

			if ($result) {
				rename($template_name, $template_name . ".".time());
				rename($template_name . ".fixed", $template_name);
			}
		}

	}

}
?>